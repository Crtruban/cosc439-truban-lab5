#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void *worker(void *arg)
{
  printf("\nHello World! I am thread %d\n", arg);
}
int main(int argc, char* argv[])
{
  int value = atoi(argv[1]);
  while((value > 10) | (value<1))
    {
      printf("You must enter a value between 1 an 10");
      exit(1);
    }
  pthread_t id[10];
  int i;
  for (i = 0; i < value; i++)
    {
      pthread_create(&id[i], NULL, &worker, (void *) i+1);
      printf("Creating Thread %d\n", i+1);
    
    }

pthread_exit(NULL);
return 0;
}
