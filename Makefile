# the compiler: gcc for C porgram, define as g++ for C++
CC = gcc

#compiler flahs:
# -g adds debugging information to the executable file
# -Wall turns on most, but not all. compiler warnings
CFLAGS = -g -Wall -pthread

#the build target executable:
TARGET = votecounter
TARGET1 = hellothread
all: 
		$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).c | $(CC) $(CFLAGS) -o $(TARGET1) $(TARGET1).c

clean:
	$(RM) $(TARGET)
